# MAD

The implementation of the Madeus model in Python.

## Documentation

A complete documentation, including a getting started is available online at 
https://mad.readthedocs.io/en/latest/
