from mad import *
import time


class Common(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed',
            'ktb_deployed'
        ]

        self.transitions = {
            'ktb_deploy': ('initiated', 'ktb_deployed', self.ktb_deploy),
            'deploy': ('ktb_deployed', 'deployed', self.deploy)
        }

        self.dependencies = {
            'common': (DepType.PROVIDE, ['ktb_deployed', 'deployed']),
            'facts': (DepType.USE, ['ktb_deploy'])
        }

    def deploy(self):
        time.sleep(1)

    def ktb_deploy(self):
        time.sleep(1)
