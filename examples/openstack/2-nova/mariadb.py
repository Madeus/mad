from mad import *
import time

class MariaDB(Component):

    def create(self):
        self.places = [
            #'waiting',
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            #'init': ('waiting', 'initiated', self.init),
            'pull': ('initiated', 'pulled', self.pull),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'common': (DepType.USE, ['deploy']),
            'haproxy': (DepType.USE, ['deploy']),
            'mdbd': (DepType.DATA_PROVIDE, ['initiated']),
            'mariadb': (DepType.PROVIDE, ['deployed'])
        }

    def init(self):
        pass

    def pull(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(0.5)
