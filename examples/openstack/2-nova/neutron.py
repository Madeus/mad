from mad import *
import time


class Neutron(Component):

    def create(self):
        self.places = [
            # try merge waiting && initiated
            #'waiting',
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            # try merge waiting && initiated
            #'init': ('waiting', 'initiated', self.init),
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'register': ('initiated', 'pulled', self.register),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['config']),
            'mdbd': (DepType.DATA_USE, ['config']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'neud': (DepType.DATA_PROVIDE, ['initiated'])
            #'neutron': (DepType.PROVIDE, ['deployed'])
        }

    def init(self):
        pass

    def pull(self):
        time.sleep(1)

    def config(self):
        time.sleep(1)

    def register(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(1)
