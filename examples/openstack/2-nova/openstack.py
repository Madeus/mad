#!/usr/bin/env python3

from mad import *
from facts import Facts
from common import Common
from haproxy import HAProxy
from memcached import MemCached
from rabbitmq import RabbitMQ
from mariadb import MariaDB
from keystone import Keystone
from glance import Glance
from nova import Nova
from openvswitch import OpenVSwitch
from neutron import Neutron
from mada import *
import time

if __name__ == '__main__':

    # Initialize our OpenStack components:
    fac = Facts()
    com = Common()
    ha = HAProxy()
    mem = MemCached()
    rab = RabbitMQ()
    mar = MariaDB()
    key = Keystone()
    gla = Glance()
    nov = Nova()
    ovs = OpenVSwitch()
    neu = Neutron()

    # Assembly components
    ass = Assembly()
    ass.addComponent('facts', fac)
    ass.addComponent('common', com)
    ass.addComponent('haproxy', ha)
    ass.addComponent('memcached', mem)
    ass.addComponent('rabbitmq', rab)
    ass.addComponent('mariadb', mar)
    ass.addComponent('keystone', key)
    ass.addComponent('glance', gla)
    ass.addComponent('nova', nov)
    ass.addComponent('openvswitch', ovs)
    ass.addComponent('neutron', neu)

    # Assembly connections
    ass.addConnection(fac, 'facts', com, 'facts')
    ass.addConnection(fac, 'facts', ha, 'facts')
    ass.addConnection(fac, 'facts', rab, 'facts')
    ass.addConnection(fac, 'facts', mem, 'facts')
    ass.addConnection(fac, 'facts', ovs, 'facts')

    ass.addConnection(com, 'common', mar, 'common')
    ass.addConnection(ha, 'haproxy', mar, 'haproxy')
    ass.addConnection(mar, 'mariadb', key, 'mariadb')
    ass.addConnection(mar, 'mariadb', nov, 'mariadb')
    ass.addConnection(mar, 'mariadb', gla, 'mariadb')
    ass.addConnection(mar, 'mariadb', neu, 'mariadb')
    ass.addConnection(mar, 'mdbd', key, 'mdbd')
    ass.addConnection(mar, 'mdbd', nov, 'mdbd')
    ass.addConnection(mar, 'mdbd', gla, 'mdbd')
    ass.addConnection(mar, 'mdbd', neu, 'mdbd')
    ass.addConnection(mar, 'mdbd', ha, 'mdbd')

    ass.addConnection(key, 'keystone', nov, 'keystone')
    ass.addConnection(key, 'keystone', gla, 'keystone')
    ass.addConnection(key, 'keystone', neu, 'keystone')
    ass.addConnection(key, 'kstd', nov, 'kstd')
    ass.addConnection(key, 'kstd', gla, 'kstd')
    ass.addConnection(key, 'kstd', neu, 'kstd')
    ass.addConnection(key, 'kstd', ha, 'kstd')

    ass.addConnection(rab, 'rabd', neu, 'rabd')
    ass.addConnection(rab, 'rabd', gla, 'rabd')
    ass.addConnection(rab, 'rabd', nov, 'rabd')

    ass.addConnection(nov, 'novad', ha, 'novad')
    ass.addConnection(neu, 'neud', ha, 'neud')
    ass.addConnection(gla, 'glad', ha, 'glad')
    ass.addConnection(gla, 'glad', nov, 'glad')

    mada = Mada(ass)

    #mada.set_interval("mariadb", "init", 0, 1)
    mada.set_interval("mariadb", "pull", 3, 4)
    mada.set_interval("mariadb", "deploy", 74, 76)

    mada.set_interval("facts", "deploy", 4, 4)

    mada.set_interval("common", "ktb_deploy", 6, 7)
    mada.set_interval("common", "deploy", 25, 26)

    mada.set_interval("glance", "deploy", 91, 98)
    mada.set_interval("glance", "register", 24, 27)
    mada.set_interval("glance", "config", 8, 9)
    mada.set_interval("glance", "pull", 5, 6)
    # mada.set_interval("glance", "init", 0, 1)

    mada.set_interval("haproxy", "deploy", 19, 20)

    # mada.set_interval("mariadb", "init", 0, 1)
    mada.set_interval("mariadb", "pull", 4, 5)
    mada.set_interval("mariadb", "deploy", 62, 67)

    mada.set_interval("memcached", "deploy", 4, 5)

    # mada.set_interval("neutron", "init", 0, 1)
    mada.set_interval("neutron", "pull", 7, 8)
    mada.set_interval("neutron", "config", 26, 29)
    mada.set_interval("neutron", "register", 21, 27)
    mada.set_interval("neutron", "deploy", 200, 203)

    # mada.set_interval("nova", "init", 0, 1)
    mada.set_interval("nova", "pull", 8, 9)
    mada.set_interval("nova", "config", 20, 23)
    mada.set_interval("nova", "create_db", 13, 15)
    mada.set_interval("nova", "upgrade_api_db", 44, 46)
    mada.set_interval("nova", "upgrade_db", 99, 101)
    # mada.set_interval("nova", "restart", 13, 14)
    mada.set_interval("nova", "register", 51, 58)
    # mada.set_interval("nova", "cell-setup", 176, 188)
    mada.set_interval("nova", "deploy", 189, 202)

    mada.set_interval("openvswitch", "deploy", 10, 12)

    # mada.set_interval("keystone", "init", 0, 1)
    mada.set_interval("keystone", "pull", 4, 5)
    mada.set_interval("keystone", "deploy", 98, 106)

    mada.set_interval("rabbitmq", "deploy", 9, 10)

    start = time.time()
    mada.run()
    end = time.time()

    print(end - start)
