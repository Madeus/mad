from mad import *
import time


class RabbitMQ(Component):

    def create(self):
        self.places = [
            # try merge waiting && initiated
            #'waiting',
            'initiated',
            'deployed'
        ]

        self.transitions = {
            # try merge waiting && initiated
            #'init': ('waiting', 'initiated', self.init),
            'deploy': ('initiated', 'deployed', self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.USE, ['deploy']),
            #'rabbitmq': (DepType.PROVIDE, ['deployed']),
            'rabd': (DepType.DATA_PROVIDE, ['initiated'])
        }

    def init(self):
        pass

    def deploy(self):
        time.sleep(1)

