from mad import *
import time


class Nova(Component):

    def create(self):
        self.places = [
            #'waiting',
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            #'init': ('waiting', 'initiated', self.init),
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'register': ('initiated', 'pulled', self.register),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['config']),
            'mdbd': (DepType.DATA_USE, ['config']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'glad': (DepType.DATA_USE, ['config']),
            'novad': (DepType.DATA_PROVIDE, ['initiated'])
            #'nova': (DepType.PROVIDE, ['deployed'])
        }

    def init(self):
        pass

    def pull(self):
        time.sleep(1)

    def config(self):
        time.sleep(1)

    def register(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(1)
