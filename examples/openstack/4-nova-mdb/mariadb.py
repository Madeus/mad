from mad import *
import time

class MariaDB(Component):

    def create(self):
        self.places = [
            # try merge waiting && initiated
            #'waiting',
            'initiated',
            'bootstrapped',
            'restarted',
            'deployed'
        ]

        self.transitions = {
            # try merge waiting && initiated
            #'init': ('waiting', 'initiated', self.init),
            'pull': ('initiated', 'bootstrapped', self.pull),
            'bootstrap': ('initiated', 'bootstrapped', self.bootstrap),
            'restart': ('bootstrapped', 'restarted', self.restart),
            'register': ('restarted', 'deployed', self.register),
            'check': ('restarted', 'deployed', self.check)
        }

        self.dependencies = {
            'common': (DepType.USE, ['register']),
            'haproxy': (DepType.USE, ['check']),
            'mdbd': (DepType.DATA_PROVIDE, ['initiated']),
            'mariadb': (DepType.PROVIDE, ['deployed'])
        }

    def init(self):
        pass

    def pull(self):
        time.sleep(1)

    def bootstrap(self):
        time.sleep(1)

    def restart(self):
        time.sleep(2)

    def register(self):
        time.sleep(3)

    def check(self):
        time.sleep(1)

