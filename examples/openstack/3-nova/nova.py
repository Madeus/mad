from mad import *
import time


class Nova(Component):

    def create(self):
        self.places = [
            # try merge waiting && initiated
            #'waiting',
            'initiated',
            'pulled',
            'ready',
            # try merge restarted && deployed
            #'restarted',
            'deployed'
        ]

        self.transitions = {
            # try merge waiting && initiated
            #'init': ('waiting', 'initiated', self.init),
            'pull': ('initiated', 'pulled', self.pull),
            'config': ('initiated', 'pulled', self.config),
            'create_db': ('initiated', 'pulled', self.create_db),
            'register': ('initiated', 'deployed', self.register),
            'upgrade_db': ('pulled', 'ready', self.upgrade_db),
            'upgrade_api_db': ('pulled', 'ready', self.upgrade_api_db),
            # try merge restarted && deploy
            #'restart': ('ready', 'restarted', self.restart),
            #'deploy': ('restarted', 'deployed', self.deploy)
            'deploy': ('ready', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['create_db', 'upgrade_db',
                                     'upgrade_api_db']),
            'mdbd': (DepType.DATA_USE, ['create_db', 'upgrade_db',
                                      'upgrade_api_db']),
            'kstd': (DepType.DATA_USE, ['config', 'register']),
            'keystone': (DepType.USE, ['register']),
            'rabd': (DepType.DATA_USE, ['config']),
            'glad': (DepType.DATA_USE, ['config']),
            'novad': (DepType.DATA_PROVIDE, ['initiated'])
            #'nova': (DepType.PROVIDE, ['deployed'])
        }

    def init(self):
        pass

    def pull(self):
        time.sleep(1)

    def config(self):
        time.sleep(1)

    def create_db(self):
        time.sleep(1.5)

    def register(self):
        time.sleep(1)

    def upgrade_db(self):
        time.sleep(1)

    def upgrade_api_db(self):
        time.sleep(1)

    def restart(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(1)
