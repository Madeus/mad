from random import *

from mad import *
from mada import *
from examples.mada.component_templates import *
import time


if __name__ == '__main__':
    # parameter to adapt as needed
    output_folder = '/home/simon/tmp/mada/' # with trailing slash
    benchmark_name = 'test' # must be unique, used for output file name
    max_transition_duration = 100
    transition_duration_variability = 10

    # build assembly here
    c1 = SequentialProvider(2)
    c2 = SequentialUser(2)
    a = Assembly()
    a.addComponent('c1', c1)
    a.addComponent('c2', c2)
    a.addConnection(c1, 'provide0', c2, 'use0')
    a.addConnection(c1, 'provide1', c2, 'use1')

    # the rest of the script does not need to be modified

    # set intervals
    mada = Mada(a)
    #transitions = set()
    #for c in a.get_components():
    #    for t in c.get_transitions():
    #        d1 = 1 + randrange(max_transition_duration)
    #        d2 = randrange(transition_duration_variability)
    #        mada.set_interval(c.getname(), t, d1, d1+d2)

    # deployment: all final places
    deployment = []
    for c in a.get_components():
        for p in c.get_places().values():
            if len(p.get_outputdocks()) == 0:
                deployment.append((c, p.getname()))
    mada.add_deployment('final', deployment)

    # parallelism (whole assembly)
    mada.clear_verification_goals()
    mada.parallelism([])
    mada.run(output_folder + benchmark_name + "_parallelism.cts")

    # deployability of final places (no maximum duration)
    mada.clear_verification_goals()
    mada.deployability('final')
    mada.run(output_folder + benchmark_name + "_deployability_true.cts")

    # gantt boundaries
    mada.clear_verification_goals()
    mada.gantt_boundaries('final')
    mada.run(output_folder + benchmark_name + "_gantt_boundaries.cts")

    # forbidden state: pick one place per component, either marked or unmarked
    mada.clear_verification_goals()
    marked = [(c1, 'p0'), (c2, 'p1')]
    unmarked = []
    mada.forbidden(marked, unmarked)
    mada.run(output_folder + benchmark_name + "_forbidden_true.cts")

    # sequentiality
    mada.clear_verification_goals()
    # pick N distinct compoenents, and a transition in each, form a sequence
    mada.sequentiality([(c2, 't0'), (c1, 't0')])
    mada.run(output_folder + benchmark_name + "_sequentiality_false.cts")
