from mad import *
from mada import *
from examples.mada.example1 import *
import time


class Example2(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            't1': ('initiated', 'deployed', self.t1)
        }

        self.dependencies = {
            'port1': (DepType.DATA_PROVIDE, ['deployed']),
        }

    def t1(self):
        time.sleep(1)


if __name__ == '__main__':
    ex1 = Example1()
    ex2 = Example2()

    ass = Assembly()
    # not connected components
    ass.addComponent('ex1', ex1)
    ass.addComponent('ex2', ex2)

    mada = Mada(ass)
    mada.add_deployment('deployment1', [(ex1, 'deployed'), (ex2, 'deployed')])
    mada.forbidden([(ex1, 'initiated'), (ex2, 'deployed')], [])
    mada.deployability('deployment1', None)
    mada.sequentiality([(ex1, 't1'), (ex2, 't1')])
    mada.parallelism([ex2])
    mada.run()