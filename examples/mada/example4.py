from mad import *
from mada import *
from examples.mada.example1 import *
from examples.mada.example2 import *
from examples.mada.example3 import *
import time


class Example4(Component):

    def create(self):
        self.places = [
            'initiated',
            'p1',
            'p2',
            'p3',
            'p4',
            'deployed'
        ]

        self.transitions = {
            't1': ('initiated', 'p1', self.t1),
            't2': ('initiated', 'p2', self.t2),
            't3': ('p1', 'p3', self.t3),
            't4': ('p1', 'p4', self.t4),
            't5': ('p2', 'p4', self.t5),
            't6': ('p3', 'deployed', self.t6),
            't7': ('p4', 'deployed', self.t7)
        }

        self.dependencies = {
            'port2': (DepType.PROVIDE, ['p1', 'p2', 'p3', 'p4'])
        }

    def t1(self):
        time.sleep(1)

    def t2(self):
        time.sleep(1)

    def t3(self):
        time.sleep(1)

    def t4(self):
        time.sleep(1)

    def t5(self):
        time.sleep(1)

    def t6(self):
        time.sleep(1)

    def t7(self):
        time.sleep(1)


if __name__ == '__main__':
    #ex1 = Example1()
    #ex2 = Example2()
    ex3 = Example3()
    ex32 = Example3()
    ex4 = Example4()

    ass = Assembly()
    #ass.addComponent('ex1', ex1)
    #ass.addComponent('ex2', ex2)
    ass.addComponent('ex3', ex3)
    ass.addComponent('ex32', ex32)
    ass.addComponent('ex4', ex4)
    #ass.addConnection(ex2, 'port1', ex3, 'port1')
    ass.addConnection(ex3, 'port2', ex4, 'port2')
    ass.addConnection(ex32, 'port2', ex4, 'port2')

    mada = Mada(ass)
    mada.run()