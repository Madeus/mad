from random import *

from mad import *
from mada import *
from examples.mada.component_templates import *
import time


if __name__ == '__main__':
    # parameter to adapt as needed
    output_folder = '/home/simon/tmp/mada/' # with trailing slash
    benchmark_name = 'pp+pu+3hbis' # must be unique, used for output file name
    max_transition_duration = 100
    transition_duration_variability = 10
    sequence_length = 2

    # could be useful if we want to re-run benchmarks with manual modifications
    seed(437526913478289)

    # build assembly here
    c1 = ParallelProvider(6)
    c2 = Hybrid(5)
    c3 = Hybrid(6)
    c4 = Hybrid(4)
    c5 = ParallelUser(5)
    a = Assembly()
    a.addComponent('c1', c1)
    a.addComponent('c2', c2)
    a.addComponent('c3', c3)
    a.addComponent('c4', c4)
    a.addComponent('c5', c5)
    a.addConnection(c1, 'provide0', c2, 'use0')
    a.addConnection(c1, 'provide1', c2, 'use1')
    a.addConnection(c1, 'provide2', c2, 'use2')
    a.addConnection(c1, 'provide3', c2, 'use3')
    a.addConnection(c1, 'provide4', c2, 'use4')
    a.addConnection(c1, 'provide5', c3, 'use0')
    a.addConnection(c2, 'provide0', c3, 'use1')
    a.addConnection(c2, 'provide1', c3, 'use2')
    a.addConnection(c2, 'provide2', c3, 'use3')
    a.addConnection(c2, 'provide3', c3, 'use4')
    a.addConnection(c2, 'provide4', c3, 'use5')
    a.addConnection(c3, 'provide0', c4, 'use0')
    a.addConnection(c3, 'provide1', c4, 'use1')
    a.addConnection(c3, 'provide2', c4, 'use2')
    a.addConnection(c3, 'provide3', c4, 'use3')
    a.addConnection(c3, 'provide4', c4, 'use0')
    a.addConnection(c3, 'provide5', c5, 'use0')
    a.addConnection(c4, 'provide0', c5, 'use1')
    a.addConnection(c4, 'provide1', c5, 'use2')
    a.addConnection(c4, 'provide2', c5, 'use3')
    a.addConnection(c4, 'provide3', c5, 'use4')
    for c in a.get_components():
        assert c.check_connections()

    # the rest of the script does not need to be modified

    # set intervals
    mada = Mada(a)
    transitions = set()
    for c in a.get_components():
        for t in c.get_transitions():
            d1 = 1 + randrange(max_transition_duration)
            d2 = randrange(transition_duration_variability)
            mada.set_interval(c.getname(), t, d1, d1+d2)

    # deployment: all final places
    deployment = []
    for c in a.get_components():
        for p in c.get_places().values():
            if len(p.get_outputdocks()) == 0:
                deployment.append((c, p.getname()))
    mada.add_deployment('final', deployment)

    # parallelism (whole assembly)
    mada.clear_verification_goals()
    mada.parallelism([])
    mada.run(output_folder + benchmark_name + "_parallelism.cts")

    # deployability of final places (no maximum duration)
    mada.clear_verification_goals()
    mada.deployability('final')
    mada.run(output_folder + benchmark_name + "_deployability.cts")

    # gantt boundaries
    mada.clear_verification_goals()
    mada.gantt_boundaries('final', False)
    mada.run(output_folder + benchmark_name + "_criticalmin.cts")
    mada.clear_verification_goals()
    mada.gantt_boundaries('final', True)
    mada.run(output_folder + benchmark_name + "_criticalmax.cts")

    # forbidden state: pick one place per component, either marked or unmarked
    mada.clear_verification_goals()
    marked = []
    unmarked = []
    for c in a.get_components():
        p = choice(list(c.get_places().values()))
        if getrandbits(1):
            marked.append((c, p.getname()))
        else:
            unmarked.append((c, p.getname()))
    mada.forbidden(marked, unmarked)
    mada.run(output_folder + benchmark_name + "_forbidden.cts")

    # sequentiality
    mada.clear_verification_goals()
    # pick N distinct compoenents, and a transition in each, form a sequence
    components = list(a.get_components())
    shuffle(components)
    components = components[:sequence_length]
    sequence = list(map(lambda c: (c, choice(list(c.get_transitions()))), components))
    mada.sequentiality(sequence)
    mada.run(output_folder + benchmark_name + "_sequentiality.cts")
