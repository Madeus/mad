from mad import *
from mada import *
from examples.mada.example1 import *
from examples.mada.example2 import *
import time


class Example3(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            't1': ('initiated', 'deployed', self.t1)
        }

        self.dependencies = {
            #'port1': (DepType.DATA_USE, ['t1']),
            'port2': (DepType.USE, ['t1'])
        }

    def t1(self):
        time.sleep(1)


if __name__ == '__main__':
    ex1 = Example1()
    ex2 = Example2()
    ex3 = Example3()

    ass = Assembly()
    ass.addComponent('ex1', ex1)
    ass.addComponent('ex2', ex2)
    ass.addComponent('ex3', ex3)
    ass.addConnection(ex2, 'port1', ex3, 'port1')

    mada = Mada(ass)

    mada.add_deployment('deployment1', [(ex1, 'deployed'), (ex2, 'deployed'), (ex3, 'deployed')])
    mada.deployability('deployment1', None)
    mada.run()