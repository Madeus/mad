from mad import *
from mada import *
import time


class Example1(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            't1': ('initiated', 'deployed', self.t1),
            't2': ('initiated', 'deployed', self.t2)
        }

        self.dependencies = {}

    def t1(self):
        time.sleep(1)

    def t2(self):
        time.sleep(1)


if __name__ == '__main__':
    ex1 = Example1()

    ass = Assembly()
    ass.addComponent('ex1', ex1)

    mada = Mada(ass)
    mada.run()