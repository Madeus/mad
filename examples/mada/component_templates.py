from mada import *
from random import shuffle
import time


class ParallelUser(Component):

    def __init__(self, n_use_ports):
        self.places = ['p0', 'p1']
        self.transitions = {}
        self.dependencies = {}
        for i in range(n_use_ports):
            self.transitions['t' + str(i)] = ('p0', 'p1', self.t1)
            self.dependencies['use' + str(i)] = (DepType.DATA_USE, ['t' + str(i)])
        super().__init__()

    def create(self):
        # everything is done in init
        pass

    def t1(self):
        time.sleep(1)


class SequentialUser(Component):

    def __init__(self, n_use_ports):
        self.places = ['p0']
        self.transitions = {}
        self.dependencies = {}
        for i in range(n_use_ports):
            self.places.append('p' + str(i + 1))
            self.transitions['t' + str(i)] = ('p' + str(i), 'p' + str(i + 1), self.t1)
            self.dependencies['use' + str(i)] = (DepType.DATA_USE, ['t' + str(i)])
        super().__init__()

    def create(self):
        # everything is done in init
        pass

    def t1(self):
        time.sleep(1)


class ParallelProvider(Component):

    def __init__(self, n_provide_ports):
        self.places = ['p0']
        self.dependencies = {}
        self.transitions = {}
        for i in range(n_provide_ports):
            self.places.append('p' + str(i + 1))
            self.transitions['t' + str(i)] = ('p0', 'p' + str(i + 1), self.t1)
            self.dependencies['provide' + str(i)] = (DepType.DATA_PROVIDE, ['p' + str(i + 1)])
        super().__init__()

    def create(self):
        # everything is done in init
        pass

    def t1(self):
        time.sleep(1)


class SequentialProvider(Component):

    def __init__(self, n_provide_ports):
        self.places = ['p0']
        self.dependencies = {}
        self.transitions = {}
        for i in range(n_provide_ports):
            self.places.append('p' + str(i + 1))
            self.transitions['t' + str(i)] = ('p' + str(i), 'p' + str(i + 1), self.t1)
            self.dependencies['provide' + str(i)] = (DepType.DATA_PROVIDE, ['p' + str(i + 1)])
        super().__init__()

    def create(self):
        # everything is done in init
        pass

    def t1(self):
        time.sleep(1)


class Hybrid(Component):

    def __init__(self, n_ports, width=3):
        p = 1
        t = 0
        self.places = ['p0']
        self.transitions = {}
        self.dependencies = {}
        for h in range(n_ports):
            self.places.append('p' + str(p))
            self.places.append('p' + str(p + 1))
            self.dependencies['provide' + str(h)] = (DepType.DATA_PROVIDE, ['p' + str(p + 1)])
            self.transitions['t' + str(t)] = ('p' + str(p - 1), 'p' + str(p), self.t1)
            self.dependencies['use' + str(h)] = (DepType.DATA_USE, ['t' + str(t)])
            t += 1
            for w in range(width):
                self.transitions['t' + str(t)] = ('p' + str(p), 'p' + str(p + 1), self.t1)
                t += 1
            p += 2
        # adds one parallel transition from initial to final place
        self.transitions['t' + str(t)] = ('p0', 'p' + str(p - 1), self.t1)
        super().__init__()

    def create(self):
        # everything is done in init
        pass

    def t1(self):
        time.sleep(1)
