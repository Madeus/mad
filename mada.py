#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
.. module:: mada
   :synopsis: this file contains the Mada class.
"""

import os
from mad import *

class Mada(object):
    """
    This class is the MAD Analyser plugin of Mad
    This class uses an assembly and a set of properties to generate a
    Petri net (Romeo format) and its associated temporal logic properties.
    Finally it runs the Romeo model checker to verify the properties of the
    Petri net.
    """

    def __init__(self, a):
        self.intervals = {}
        self.deployments = {}
        self.deployabilities = []
        self.sequentialities = []
        self.forbidden_states = []
        self.assembly = a
        self.check_parallelism = None
        self.check_gantt_boundaries = None
        self.gantt_boundaries_maximize = False

    """
    BUILD PROPERTIES
    """

    def set_interval(self, component, transition, mini, maxi):
        tname = component + "_" + transition
        self.intervals[tname] = (mini, maxi)

    def add_deployment(self, name, places):
        # places must be a collection of (component, place_name)
        for p in places:
            assert p[1] in p[0].get_places().keys()
        self.deployments[name] = places

    # SR: there was an additional argument 'traces', what was the intention?
    def deployability(self, deployment_name, interval=0):
        assert deployment_name in self.deployments
        self.deployabilities.append((deployment_name, interval))

    def sequentiality(self, transitions):
        # transitions must be a sequences of (component, transition_name)
        assert len(transitions) > 1
        for t in transitions:
            assert t[1] in t[0].get_transitions().keys()
        self.sequentialities.append(transitions)

    def parallelism(self, components):
        # compoment is a list of components to include, or the empty list to check the whole assembly
        self.check_parallelism = components

    def gantt_boundaries(self, deployment_name, maximize = False):
        assert deployment_name in self.deployments
        self.check_gantt_boundaries = deployment_name
        self.gantt_boundaries_maximize = maximize

    def forbidden(self, marked, unmarked):
        for p in marked:
            assert p[1] in p[0].get_places().keys()
        for p in unmarked:
            assert p[1] in p[0].get_places().keys()
        self.forbidden_states.append((marked, unmarked))

    def clear_verification_goals(self):
        self.deployabilities = []
        self.sequentialities = []
        self.forbidden_states = []
        self.check_parallelism = None
        self.check_gantt_boundaries = None

    """
    BUILD PETRI NET
    """

    def petri_net(self):

        # variables in cts file
        # any Madeus place, dock, transition or a connection becomes a place in
        # the Petri net. Additional places are added to handle groups.
        variables = "initially \n{\n"

        # transitions in cts file
        # links between created places
        pnettransitions = ""

        connections = self.assembly.get_connections()
        #print(connections)
        # number of uses per service provide port
        nbuse = {}
        for conn in connections:
            if conn[1].gettype() == DepType.PROVIDE:
                connname = conn[0].getname() + "_" + conn[1].getname()
                if connname in nbuse.keys():
                    nbuse[connname] += 1
                else:
                    nbuse[connname] = 1
            if conn[3].gettype() == DepType.PROVIDE:
                connname = conn[2].getname() + "_" + conn[3].getname()
                if connname in nbuse.keys():
                    nbuse[connname] += 1
                else:
                    nbuse[connname] = 1

        # variables of observer subnet for sequentiality properties
        watched_transitions = set()
        if self.sequentialities:
            variables += "int badsequence = 0;\n"
            for s in self.sequentialities:
                for (component, transition_name) in s:
                    tname = component.getname() + "_" + transition_name
                    watched_transitions.add(tname)
            for w in watched_transitions:
                variables += "int " + w + "_fired = 0;\n"
                variables += "int " + w + "_neverfired = 1;\n"

        # variables and transitions for components
        for comp in self.assembly.get_components():
            #print("+++ component " + comp.getname())

            #####
            # variables for provide ports : provide, in-use, not-used
            # (no variable for use ports represented by set of
            # pntransitions)
            #####
            # Build a dictionary of groups containing two lists :
            # input and output places of groups
            #####
            dependencies = comp.get_dependencies()
            transitions = comp.get_transitions()
            entry_group = {}
            leaving_group = {}
            for dep in dependencies:
                if dependencies[dep].gettype() == DepType.PROVIDE or \
                        dependencies[dep].gettype() == DepType.DATA_PROVIDE:
                    depname = comp.getname() + "_" + dependencies[dep].getname()
                    # provide variable
                    variables += "int " + depname + " = 0;\n"
                    if dependencies[dep].gettype() == DepType.PROVIDE:
                        # in-use variable
                        variables += "int " + depname+ "_inuse = 0;\n"
                        # not-used variable
                        if depname in nbuse.keys():
                            variables += "int " + depname + "_notused = " + \
                                         str(nbuse[depname]) + ";\n"

                    # dictionary
                    if dependencies[dep].gettype() == DepType.PROVIDE:
                        all_places = dependencies[dep].getbindings()
                        entry_group[depname] = []
                        leaving_group[depname] = []
                        for t in transitions:
                            # src place
                            src = transitions[t].get_src_dock().getplace()
                            # dst place
                            dst = transitions[t].get_dst_dock().getplace()
                            if src in all_places and dst not in all_places:
                                if src not in leaving_group[depname]:
                                    leaving_group[depname].append(src)
                            elif dst in all_places and src not in all_places:
                                if dst not in entry_group[depname]:
                                    entry_group[depname].append(dst)
                    #print(entry_group)
                    #print(leaving_group)
                    #####

            places = comp.get_places()
            for p in places:

                pname = comp.getname() + "_" + places[p].getname()

                #####
                # provides associated to the current transition
                #####
                provides = places[p].get_provides()
                #####

                #####
                # variables for idocks
                #####
                idocks = places[p].get_inputdocks()
                idocks_names = []
                for id in range(len(idocks)):
                    # name of the dock
                    dname = pname + "_id" + str(id)
                    idocks_names.append(dname)
                    # variable for the dock
                    variables += "int " + dname + " = 0;\n"
                #####

                #####
                # variable for the place
                #####
                if len(idocks) > 0:
                    variables += "int " + pname + " = 0;\n"
                else:
                    # initial place
                    variables += "int " + pname + " = 1;\n"
                #####

                #####
                # variables for odocks
                #####
                odocks_names = []
                odocks = places[p].get_outputdocks()
                for od in range(len(odocks)):
                    # name of the dock
                    dname = pname + "_od" + str(od)
                    odocks_names.append(dname)
                    # variable for the dock
                    variables += "int " + dname + " = 0;\n"
                #####

                #####
                # pntransition between idocks and place
                # if needed addition of data-provides associated to the
                # destination place
                # join/merge in groups
                #####
                if len(idocks_names) > 0:
                    tname = pname + "_idocks"
                    ### inputs of the pntransition
                    trans = "transition " + tname + " [0,0] " + " when ("
                    for idn in idocks_names:
                        if idn == idocks_names[-1]:
                            trans += idn + "==1"
                        else:
                            trans += idn + "==1 && "
                    # if merge inside a group
                    # happens when place have more than one idock
                    if len(idocks) > 1:
                        #for dep in dependencies:
                            # if this place is bound to a group
                        #    if dependencies[dep].gettype() == DepType.PROVIDE\
                        #     and places[p] in dependencies[dep].getbindings():
                        for pr in provides:
                            if pr.gettype() == DepType.PROVIDE:
                                prname = comp.getname() + "_" + pr.getname()
                                # not for the entry places of groups
                                if prname not in entry_group.keys():
                                    # additional token movements from the provide
                                    # port to the pntransition
                                    trans += " && " + prname + ">=" + str(len(idocks))
                    trans += "){"
                    ###
                    ### outputs of the pntransition
                    for idn in idocks_names:
                        trans += idn + "=0; "
                    # data provide ports to 1 (data only)
                    provides = places[p].get_provides()
                    for pr in provides:
                        prname = comp.getname() + "_" + pr.getname()
                        if pr.gettype() == DepType.DATA_PROVIDE:
                            trans += prname + "=1; "
                        # if the place is an entry point of a group add
                        # token to provide
                        elif prname in entry_group.keys():
                            for entry_pr in entry_group[prname]:
                                if entry_pr.getname() == p:
                                    trans += prname + "=" + prname + "+1; "
                    # if merge inside a group
                    # happens when place have more than one idock
                    if len(idocks) > 1:
                        #for dep in dependencies:
                            # if this place is bound to a group
                        #   if dependencies[dep].gettype() == DepType.PROVIDE\
                        #     and places[p] in dependencies[dep].getbindings():
                        for pr in provides:
                            if pr.gettype() == DepType.PROVIDE:
                                prname = comp.getname() + "_" + pr.getname()
                                # from the pntransition to the provide port
                                # merge input docks
                                # a single token is put in the provide
                                # not for the entry places of groups
                                if prname not in entry_group.keys():
                                    trans += prname + "=" + prname + "-" + str(len(
                                        idocks) - 1) + "; "
                    # pnplace representing the Madeus place to 1
                    trans += pname + "=1;}"
                    ###
                    pnettransitions += trans + "\n"
                #####

                #####
                # pn transition(s) between place and odocks 1/2
                # leaving a group when the provide port is not used
                # forks in groups
                #####
                quit_groups = []
                if len(odocks_names) > 0:
                    tname = pname + "_odocks"
                    ### inputs of pntransition
                    trans = "transition " + tname +  " [0,0] " + " when (" + pname + "==1"
                    #for dep in dependencies:
                        # if this place is bound to a group
                    #    if dependencies[dep].gettype() == DepType.PROVIDE \
                    #        and places[p] in dependencies[dep].getbindings():
                    for pr in provides:
                        if pr.gettype() == DepType.PROVIDE:
                            prname = comp.getname() + "_" + pr.getname()
                            ### if leaving the place = leave a group
                            if prname in leaving_group.keys() and \
                                    places[p] in leaving_group[prname]:
                                # remember the provide for later
                                quit_groups.append(pr)
                                trans += " && " + prname + ">=1"
                                trans += " && " + prname + "_notused>=" + str(nbuse[
                                    prname])
                            ### if fork inside a group
                            ### happens when place have more than one odock
                            if len(odocks) > 1:
                                prname = comp.getname() + "_" + dependencies[dep].getname()
                                # additional token movements from the provide
                                # port to the pntransition
                                trans += " && " + prname + ">=1"
                    trans += "){"
                    ###
                    ### outputs of pntransition
                    for odn in odocks_names:
                        trans += odn + "=1; "
                        # if merge inside a group
                        # happens when place have more than one idock
                    if len(odocks) > 1:
                        #for dep in dependencies:
                            # if this place is bound to a group
                        #    if dependencies[dep].gettype() == DepType.PROVIDE\
                        #            and places[p] in dependencies[
                        # dep].getbindings():
                        for pr in provides:
                            if pr.gettype() == DepType.PROVIDE:
                                prname = comp.getname() + "_" + pr.getname()
                                # from the pntransition to the provide port
                                # len(odocks) tokens are put in the provide
                                trans += prname + "=" + prname + "+" + \
                                         str(len(odocks)-1) + "; "
                    trans += pname + "=0;}"
                    pnettransitions += trans + "\n"
                    ###
                #####

                #####
                # pn transition(s) between place and odocks 2/2
                # second transition possible when leaving a group
                # leaving a group when the provide is still in use
                #####
                # create this additional pn transition only if the place quits
                # the group
                if len(quit_groups) > 0:
                    if len(odocks_names) > 0:
                        tname = pname + "_odocks_group"
                        ### inputs of pntransition
                        trans = "transition " + tname + " [0,0] " + " when (" + pname + "==1"
                        for pr in quit_groups:
                            prname = comp.getname() + "_" + pr.getname()
                            # under use
                            trans += " && " + prname + "_inuse>=1"
                            # if >=2 we can leave and the provide will
                            # still be guaranteed
                            trans += " && " + prname + ">=2"
                        trans += "){"
                        ###
                        ### outputs of pntransition
                        for odn in odocks_names:
                            trans += odn + "=1; "
                            # if merge inside a group
                            # happens when place have more than one idock
                        for pr in quit_groups:
                            prname = comp.getname() + "_" + pr.getname()
                            trans += prname + "=" + prname + "-1;"
                        trans += pname + "=0;}"
                        pnettransitions += trans + "\n"
                        ###
                #####

            for t in transitions:
                tname = comp.getname() + "_" + transitions[t].getname()
                variables += "int " + tname + " = 0;\n"

                #####
                # Petri net transition between t and its source dock
                # add use connections in these pntransitions
                # not-used, in-use places also
                #####
                sd = transitions[t].get_src_dock()
                place_sd = sd.getplace()
                # rebuild the dock name
                sdname = ""
                for od in range(len(place_sd.get_outputdocks())):
                    odock = place_sd.get_outputdocks()[od]
                    if odock == sd:
                        sdname = comp.getname() + "_" + place_sd.getname() + \
                                 "_od" + str(od)
                tname1 = sdname + "_" + tname
                trans1 = "transition " + tname1 +  " [0,0] " + " when (" + sdname + " == 1"
                # use ports
                uses = transitions[t].get_uses()
                #if len(uses) > 0:
                #    trans1 += " && "
                use_prov = {}
                for u in uses:
                    # need name of provide port
                    prname = ""
                    for conn in connections:
                        if conn[1] == u:
                            prname = conn[2].getname() + "_" + conn[3].getname()
                            break
                        elif conn[3] == u:
                            prname = conn[0].getname() + "_" + conn[1].getname()
                            break
                    use_prov[u] = prname
                    # use prname in pntransition
                    if u == uses[-1]:
                        trans1 += " && " + prname + " >=1"
                        if u.gettype() == DepType.USE:
                            trans1 += " && " + prname + "_notused >= 1"
                    else:
                        trans1 += " && " + prname + " >=1"
                        if u.gettype() == DepType.USE:
                            trans1 += " && " + prname + "_notused >= 1"
                        #else :
                        #    trans1 += " && "
                trans1 += "){"
                for u in uses:
                    if u.gettype() == DepType.USE:
                        trans1 += use_prov[u] + "_inuse " + "=" + use_prov[u]\
                                  + "_inuse " + "+1;"
                        trans1 += use_prov[u] + "_notused " + "=" + use_prov[
                            u] + "_notused " + "-1;"
                trans1 += sdname + "=0; " + tname + "=1;}"
                # the provide port keeps its token
                # no need for prname = 0 nor prname = 1
                pnettransitions += trans1 + "\n"
                #####

                #####
                # Petri net transition between t and its dest dock
                # update in-use place + not-used place
                #####
                dd = transitions[t].get_dst_dock()
                place_dd = dd.getplace()
                # rebuild the dock name
                ddname = ""
                for id in range(len(place_dd.get_inputdocks())):
                    idock = place_dd.get_inputdocks()[id]
                    if idock == dd:
                        ddname = comp.getname() + "_" + place_dd.getname() + \
                                 "_id" + str(id)
                tname2 = ddname + "_" + tname
                if tname in self.intervals.keys():
                    trans2 = "transition " + tname2 + " [" + str(self.intervals[
                        tname][0]) + "," + str(self.intervals[tname][1]) + "] when " \
                                            "(" + tname + " == 1"
                else:
                    trans2 = "transition " + tname2 +  " [0,0] " + " when (" \
                             + tname + " == 1"
                for u in uses:
                    if u.gettype() == DepType.USE:
                        trans2 += " && " + use_prov[u] + "_inuse >= 1"
                trans2 += "){"
                for u in uses:
                    if u.gettype() == DepType.USE:
                        trans2 += use_prov[u] + "_inuse = " + use_prov[u] + \
                                  "_inuse - 1;"
                        trans2 += use_prov[u] + "_notused =" + use_prov[u] + \
                                  "_notused + 1;"
                # observer subnet for sequentiality properties
                if tname in watched_transitions:
                    trans2 += tname + "_fired=1; "
                    trans2 += tname + "_neverfired=0; "
                trans2 += ddname + "=1; " + tname + "=0;}"
                pnettransitions += trans2 + "\n"
                #####

        variables += "}\n"

        # pn-transitions for sequentiality subnet
        for s in self.sequentialities:
            for i in range(len(s) - 1):
                (c1, t1) = s[i]
                (c2, t2) = s[i+1]
                name1 = c1.getname() + "_" + t1
                name2 = c2.getname() + "_" + t2
                pnettransitions += "transition " + name1 + "_" + name2 + "_order [0,0]  when (" + name1 +\
                                   "_neverfired == 1 && " + name2 + "_fired == 1){badsequence=1;}\n"

        goals = ""
        # deployability
        for d in self.deployabilities:
            places = self.deployments[d[0]]
            interval = d[1]
            prop = []
            for (component, place_name) in places:
                pname = component.getname() + "_" + place_name
                prop.append(pname + "==1")
            if interval == 0:
                goals += "check AF (" + " && ".join(prop) + ")\n"
            # TODO what is the property when an interval is specified?

        # forbidden state
        for (marked, unmarked) in self.forbidden_states:
            prop = []
            for (component, place_name) in marked:
                pname = component.getname() + "_" + place_name
                prop.append(pname + "==0")
            for (component, place_name) in unmarked:
                pname = component.getname() + "_" + place_name
                prop.append(pname + "==1")
            goals += "check AG (" + " || ".join(prop) + ")\n"

        # sequentiality
        if self.sequentialities:
            goals += "check AG (badsequence==0)\n"

        # parallelism
        if self.check_parallelism is not None:
            if self.check_parallelism:
                components = self.check_parallelism
            else:
                components = self.assembly.get_components()
            prop = []
            for c in components:
                for t in c.get_transitions():
                    prop.append(c.getname() + "_" + t)
            goals += "check maxv (" + " + ".join(prop) + ")"

        # critical path
        if self.check_gantt_boundaries is not None:
            places = self.deployments[self.check_gantt_boundaries]
            prop = []
            for (component, place_name) in places:
                pname = component.getname() + "_" + place_name
                prop.append(pname + "==1")
            if self.gantt_boundaries_maximize:
                goals += "cost_rate -1\ncheck[neg_costs] mincost (" + " && ".join(prop) + ")"
            else:
                goals += "cost_rate 1\ncheck[timed_trace, absolute] mincost (" + " && ".join(prop) + ")"

        return variables + pnettransitions + goals

    def temporal_logic(self):
        for dep in self.deployabilities:
            print("deployability")
        for seq in self.sequentialities:
            print("sequentiality")
        # parallelism
        # boundaries

    """
    RUN ROMEO
    """

    def run(self, output_path=None):
        output = self.petri_net()
        if output_path is None:
            print(output)
        else:
            ##### write file
            cts_file = open(output_path, 'w')
            cts_file.write(output)
            cts_file.close()